# This Repository Has Moved
Foundry Virtual Tabletop has moved its source code repositories to GitHub. This is no longer the correct origin URL for the `worldbuilding` repository. The new correct origin URL is https://github.com/foundryvtt/worldbuilding. To switch your existing local version of the repository, please execute the following commands:
```
git remote set-url origin https://github.com/foundryvtt/worldbuilding.git
git fetch --all
```